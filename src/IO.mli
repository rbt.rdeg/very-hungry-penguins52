(**This module contains the parsing of the txt files*)

(**Takes in argument the name of a txt file and returns
 * the grid of cell and the array of the penguins' positions of 
 * 26 players (the list can be empty : if it is the case then)
 * this player must be ignored *)
val parse_map : string -> Move.elt Move.grid * (int*int) list array

(** Takes the name of a txt file and returns the number of player in 
 * the map *)
val nb_player : string -> int 

(** Returns the list of penguins pos for each player*)
val penguins_pos : string -> (Move.pos list) list

(** Returns the list of characters used for a player*)
val char_players : string -> char list 
