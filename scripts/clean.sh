#!/usr/bin/env sh

set -eu

(
  cd "$(dirname "$0")"/../

  # shellcheck disable=SC2046
  eval $(opam config env)

  rm -f ./*.data ./*.log ./*.native _tags ./src/*.mldylib ./src/*.mllib ./src/*.mldylib ./src/META ./*.odocl
  rm -rf _build ./*.docdir
)
